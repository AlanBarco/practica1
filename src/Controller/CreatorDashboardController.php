<?php

namespace App\Controller;

use App\Entity\Curso;
use App\Form\RegisterCursoType;
use App\Repository\CursoRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreatorDashboardController extends AbstractController
{
    /**
     * @Route("/creator/dashboard", name="app_creator_dashboard")
     */
    public function index(Request $request, CursoRepository $cursosRep): Response
    {
        
        return $this->render('creator_dashboard/index.html.twig', [
            'listaCursos' => $cursosRep->findAll(),
        ]);
    }
    /**
     * @Route("/creator/dashboard/register", name="app_creator_dashboard_register")
     */
    public function register(Request $request, CursoRepository $cursosRep): Response
    {
        // $user = $request->getUser();
        $curso = new Curso();
        $form = $this->createForm(RegisterCursoType::class, $curso);
        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid())
        {
            $curso = $form->getData();
            $curso->setIdUsuario(5);
            $curso->setEstado("EnConstruccion");
            $cursosRep->add($curso,true);
            //$this->addFlash("success","Registro de curso Exitoso");
            return $this->redirectToRoute('app_creator_dashboard');
        }
        return $this->render('creator_dashboard/registerCurso.html.twig', [
            'formulario' => $form->createView(),
        ]);
    }
    /**
     * @Route("/creator/dashboard/edit/{id}", name="app_creator_dashboard_edit")
     */
    public function edit(Request $request, Curso $curso, CursoRepository $cursoRep, UserRepository $userRep): Response
    {
        $mensaje = "";
        $estado = "";
        /**Obtener el objeto de tipo usuario del que esta en sesion para identificar su ID, para mapear(hacer una validacion con respecto a ese usuario) */
        $usuario = $request->getUser();
        $objUsuario = $userRep->findOneBy(["email"=>$usuario]);//el email es unico en la tabla, por lo tanto va a traer un solo registro
        $form = $this->createForm(RegisterCursoType::class, $curso, array('accion'=>'editCurso'));
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $curso = $form->getData(); //recupero datos del formulario
            if($curso->getEstado() == "Activo"){
                $totalCursos = $cursoRep->totalByUSerAndEstado($objUsuario->getId(),"Activo");
                if($totalCursos !=null && $totalCursos >=2)
                {
                    $mensaje = "Error: No puede tener más de dos cursos activos para crear";
                    $estado = "error";
                }
            }
            elseif($curso->getEstado()=="EnConstruccion"){
                $mensaje = "Error: No se puede cambiar el estado a En Desarrollo";
                    $estado = "error";
            }
            if($estado !="error"){
                $mensaje = "Curso actualizado correctamente";
                $estado = "sucess";
                $cursoRep->add($curso,true);
            }
            return $this->redirectToRoute('app_creator_dashboard');
        }
        return $this->render('creator_dashboard/registerCurso.html.twig', [
            'formulario' => $form->createView(),
        ]);

    }
    /**
     * @Route("/creator/dashboard/delete/{id}", name="app_creator_dashboard_delete")
     */
    public function remove(Curso $curso, CursoRepository $cursoRep){
        $cursoRep->remove($curso, true);
        return $this->redirectToRoute('app_creator_dashboard');
    }
}
